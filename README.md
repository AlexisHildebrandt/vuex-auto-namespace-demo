# vuex-auto-namespace-demo
This repo is a demo for an IntelliJ issue. Please checkout and view the TODOs in IntelliJ which highlight the existing and expected behaviour.

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```
