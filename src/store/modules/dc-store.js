const namespaced = true

const state = {}

const getters = {}

const mutations = {}

const actions = {
  // TODO this action is marked as used, but only coincidentally. See comment in App.vue
  async save() {},

  // TODO this action is called from another action in this module and also from the marvel module as well as from App.vue. It should not be marked "unused"
  async callSuperman(vuex, {caller = 'dc'}) {
    console.log(`superman was called by ${caller}`)
  },

  // TODO this action is referenced in App.vue. It should not be marked unused.
  async callBatman(vuex, {caller = 'dc'}) {
    console.log(`batman was called by ${caller}`)
  },

  // TODO this action is referenced in App.vue. It should not be marked unused.
  async callSupermanFromDc() {
    this.dispatch('dc/callSuperman', {caller: 'a friend at DC'})
  },
}

export default {
  namespaced,
  state,
  mutations,
  actions,
  getters,
}
