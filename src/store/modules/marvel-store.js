const namespaced = true

const state = {}

const getters = {}

const mutations = {}

const actions = {
    // TODO this action should not be marked as used. See comment in App.vue
    async save() {},

    async callSupermanFromMarvel() {
      this.dispatch('dc/callSuperman', {caller: 'marvel'})
    },
}

export default {
  namespaced,
  state,
  mutations,
  actions,
  getters,
}
